#!/usr/bin/python
# -*- coding: utf-8 -*-
import subprocess
import json

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

current_brightness = r"""echo $(xrandr --verbose -q) | grep -ioP ' [\w-]+ connected((?!connected).)*?bri.*?\s+\d+\.\d+' | sed -r 's/^ ([A-Z0-9-]+) .*?([0-9]\.[0-9]+)$/{"\1":\2}/g'"""

class Device(object):
    name = ""
    devices = []
    adj = None
    entry = None
    scale = None
    def scale_moved(self, event):
        val = int(self.scale.get_value())
        command = " xrandr --output " + self.entry.get_text() + " --brightness " + str(val * 0.01)
        subprocess.call(command, shell=True)

    def create_h_scale(self, ad1, scale_moved):
        h_scale = Gtk.Scale(
            orientation=Gtk.Orientation.HORIZONTAL, adjustment=ad1)
        h_scale.set_digits(0)
        h_scale.set_hexpand(True)
        h_scale.set_valign(Gtk.Align.START)
        h_scale.connect("value-changed", scale_moved)
        return h_scale

class DeviceWindow(Gtk.Window):
    current_settings = {}
    def get_devices_and_settings(self):
        proc = subprocess.Popen(
            current_brightness, stdout=subprocess.PIPE, shell=True)
        output = proc.stdout.read().strip().split("\n")

        for i in output:
            self.current_settings.update(json.loads(i))

    def device_creator(self):
        main_device = Device()
        for device in self.current_settings.keys():
            t_entry = Gtk.Entry()
            t_entry.set_text(device)
            dev = Device()
            dev.name=device
            dev.adj=Gtk.Adjustment(self.current_settings.get(device, 100)*100, 0, 100, 5, 10, 0)
            dev.entry=t_entry
            dev.scale=Device().create_h_scale(dev.adj, dev.scale_moved)
            main_device.devices.append(dev)
        return main_device

    def attach_devices_to_grid(self, main_devices, grid):
        for idx, dev in enumerate(main_devices.devices):
          grid.attach(dev.scale,  0, idx, 1, 2)
          grid.attach(dev.entry,  1, idx, 2, 1)

    def __init__(self):
        self.get_devices_and_settings()

        Gtk.Window.__init__(self, title="External monitor brightness manager")
        Gtk.Window.set_default_size(self, 400, 75)
        Gtk.Window.set_position(self, Gtk.WindowPosition.CENTER)

        main_devices = self.device_creator()

        label = Gtk.Label()
        label.set_text("Move the scale handles...")

        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.add(label)
        self.attach_devices_to_grid(main_devices,grid)
        self.add(grid)

window = DeviceWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
